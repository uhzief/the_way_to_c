#include <stdio.h>
#include <fcntl.h>
#include <sys/mman.h>
#include<string.h>
#include <stdlib.h>
int main()
{
    // 1. 打开一个磁盘文件
    int fd = open("./en.txt", O_RDWR);
    // 2 创建内存映射区
    void *ptr = mmap(NULL, 4000, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (ptr == MAP_FAILED)
    {
        perror("mmap");
        exit(0);
    }
    // 3 创建子进程，父进程写数据，子进程读数据
    pid_t pid = fork();
    if (pid == 0)
    {
        // 子进程, 读数据
        usleep(1); // 内存映射区不阻塞, 为了让子进程读出数据
        printf("从映射区读出的数据: %s\n", (char *)ptr);
    }
    if (pid > 0)
    {
        const char* pt = "我是你爹, 你是我儿子吗???";
        memcpy(ptr,pt,strlen(pt)+1);
    }
    // 4 释放内存映射区
    munmap(ptr,4000);
    
    return 0;
}