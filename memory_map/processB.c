#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/mman.h>
#include <fcntl.h>

int main()
{
    // 1. 打开一个磁盘文件
    int fd = open("./test.txt", O_RDWR);

    void *ptr = mmap(NULL, 4000, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

    if (ptr == MAP_FAILED)
    {
        perror("mmap");
        exit(0);
    }
    // 读内存映射区
    printf("从映射区读出的数据: %s\n", (char*)ptr);

    munmap(prt,4000);

    return 0;
}