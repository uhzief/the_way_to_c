#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<errno.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<unistd.h>

#define MAXLINE 4096

// 设置内存对其方式
#pragma pack(push,1)

struct Trade {
    double price;
    double size;
};

#pragma pack(pop)

struct Trade tradeInfo;
unsigned char buf[sizeof(struct Trade)];





int main(int argc, char** argv){

    tradeInfo.price = 0.5;
    tradeInfo.size = 100;

    memcpy((void *)buf,(void *)&tradeInfo,sizeof(struct Trade));
    printf("%d\n",buf[0]);
    int  listenfd, connfd;
    struct sockaddr_in  servaddr;
    char  buff[4096];
    int  n;
    // 创建一个套接字
    if( (listenfd = socket(AF_INET, SOCK_STREAM, 0)) == -1 ){
        printf("create socket error: %s(errno: %d)\n",strerror(errno),errno);
        return 0;
    }
    printf("%ld\n",sizeof(servaddr));
    memset(&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(6666);

    // 绑定 listenfd 到serveraddr
    if( bind(listenfd, (struct sockaddr*)&servaddr, sizeof(servaddr)) == -1){
        printf("bind socket error: %s(errno: %d)\n",strerror(errno),errno);
        return 0;
    }
    // 监听端口 是否收到连接信号
    if( listen(listenfd, 10) == -1){
        printf("listen socket error: %s(errno: %d)\n",strerror(errno),errno);
        return 0;
    }

    printf("======waiting for client's request======\n");
    while(1){
        // 等待连接
        if( (connfd = accept(listenfd, (struct sockaddr*)NULL, NULL)) == -1){
            printf("accept socket error: %s(errno: %d)",strerror(errno),errno);
            continue;
        }
        // 接受消息
        
        n = recv(connfd, buff, MAXLINE, 0);
        buff[n] = '\0';
        printf("recv msg from client: %s\n", buff);
        // 关闭连接
        close(connfd);
    }
    // 释放socket
    close(listenfd);
    return 0;
}