#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
int main()
{
    int lfd;
    struct sockaddr_in servaddr;
    {
        /* data */
    };

    // 创建socket
    if ((lfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        return 0;
    }
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(8001);
    servaddr.sin_addr.s_addr = INADDR_ANY;
    // bind
    bind(lfd, (struct servaddr *)&servaddr, sizeof(servaddr));
    // listen
    listen(lfd, 10);
    // 将监听的fd的状态检测委托给内核检测
    int maxfd = lfd;
    // 初始化检测的读集合
    fd_set read_set;
    fd_set read_temp;
    // 清零
    FD_ZERO(&read_set);
    // 将监听的lfd设置到检测的读集合中
    FD_SET(lfd, &read_set);
    // 通过select委托内核检测读集合中的文件描述符状态, 检测read缓冲区有没有数据
    // 如果有数据, select解除阻塞返回
    // 应该让内核持续检测

    while (1)
    {
        // 默认阻塞
        // rdset 中是委托内核检测的所有的文件描述符
        read_temp = read_set;
        int num = select(maxfd + 1, &read_temp, NULL, NULL, NULL);
        // rdset中的数据被内核改写了, 只保留了发生变化的文件描述的标志位上的1, 没变化的改为0
        // 只要rdset中的fd对应的标志位为1 -> 缓冲区有数据了
        // 判断
        // 有没有新连接
        if (FD_ISSET(lfd, &read_temp))
        {
            // 接受连接请求, 这个调用不阻塞
            struct sockaddr_in cliaddr;
            int cliLen = sizeof(cliaddr);
            int cfd = accept(lfd, (struct sockaddr *)&cliaddr, &cliLen);
            // 得到了有效的文件描述符
            // 通信的文件描述符添加到读集合
            // 在下一轮select检测的时候, 就能得到缓冲区的状态
            FD_SET(cfd, &read_set);
            // 重置最大的fd
            maxfd = cfd > maxfd ? cfd : maxfd;
        }
        // 没有新连接, 通信
        for (int i = 0; i < maxfd + 1; i++)
        {
            // 判断从监听的文件描述符之后到maxfd这个范围内的文件描述符是否读缓冲区有数据
            if (i != lfd && FD_ISSET(i, &read_temp))
            {
                // 接收数据
                char buf[10] = {0};
                // 一次只能接收10个字节, 客户端一次发送100个字节
                // 一次是接收不完的, 文件描述符对应的读缓冲区中还有数据
                // 下一轮select检测的时候, 内核还会标记这个文件描述符缓冲区有数据 -> 再读一次
                // 	循环会一直持续, 知道缓冲区数据被读完位置
                int len = read(i,buf,sizeof(buf));
                if(len == 0) 
                {
                     printf("客户端关闭了连接...\n");
                    // 将检测的文件描述符从读集合中删除
                    FD_CLR(i, &read_set);
                    close(i);
                }
                else if (len >0)
                {
                    // 收到了数据
                    // 发送数据
                    write(i, buf, strlen(buf)+1);
                }
                else 
                {
                    // 异常
                    perror("read");
                }
            }
        }
    }
    return 0
}