#include <stdio.h>
#include <sys/types.h>
int main()
{
    int number = 10;
    // 创建子进程，子进程从fork()之后的代码开始执行
    pid_t pid = fork();
    number += 1;
    printf("当前进程fork()的返回值: %d\n", pid);
    if (pid > 0)
    {
        // 父进程执行的逻辑
        printf("我是父进程, pid = %d\n", getpid());
        printf("父进程 number: %d\n",number);
    }
    else if (pid == 0)
    {
        // 子进程执行的逻辑
        printf("我是子进程, pid = %d, 我爹是: %d\n", getpid(), getppid());
        printf("子进程 number: %d\n",number);
    }
    else // pid =-1
    {
        // 子进程执行的逻辑
        printf("创建子进程失败");
    }
    // 不加判断, 父子进程都会执行这个循环
    for (int i = 0; i < 5; ++i)
    {
        printf("pid:%d\n",pid);
        printf("%d\n", i);
    }
    return 0;
}