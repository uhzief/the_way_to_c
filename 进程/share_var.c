// 父子进程之间可以通过全局变量互动，实现交替数数的功能吗?

// number.c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

// 定义全局变量
int number = 10;

int main()
{
    printf("创建子进程之前 number = %d\n", number);

    pid_t pid = fork();
    // 父子进程都会执行这一行
    printf("当前进程fork()的返回值: %d\n", pid);

    //如果是父进程
    if(pid > 0)
    {
        printf("我是父进程, pid = %d, number = %d\n", getpid(), ++number);
        printf("父进程的父进程(终端进程), pid = %d\n", getppid());
        sleep(1);
    }
    else if(pid == 0)
    {
        // 子进程
        number += 100;
        printf("我是子进程, pid = %d, number = %d\n", getpid(), number);
        printf("子进程的父进程, pid = %d\n", getppid());
    }

    return 0;
}

// 两个进程中是不能通过全局变量实现数据交互的，
// 因为每个进程都有自己的地址空间，两个同名全局变量存储在不同的虚拟地址空间中，
// 二者没有任何关联性。如果要进行进程间通信需要使用：
// 管道，共享内存，本地套接字，内存映射区，消息队列等方式。
