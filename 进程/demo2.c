// 练习:只允许父进程创建子进程

#include<sys/types.h>
#include<stdio.h>

int main()
{
    pid_t pid ;

    for (int i=0;i<3;i++)
    {
        pid = fork();
        if (pid ==0)
        {
            // 不让子进程执行
            break;
        }
        else if (pid==-1)
        {
            printf("fork error");
        }
    }
    if (pid >0)
    {
         printf("我是父进程 pid: %d\n", getpid());
    }
    else if (pid ==0) 
    {
        printf("我是子进程 pid: %d\n", getpid());
    }
    
}